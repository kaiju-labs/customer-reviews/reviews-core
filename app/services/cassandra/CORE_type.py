class BaseType:
    def __str__(self):
        pass


class BIGINT(BaseType):
    def __str__(self):
        return "BIGINT"


class BLOB(BaseType):
    def __str__(self):
        return "BLOB"


class BOOLEAN(BaseType):
    def __str__(self):
        return "BOOLEAN"


class COUNTER(BaseType):
    def __str__(self):
        return "COUNTER"


class DATE(BaseType):
    def __str__(self):
        return "DATE"


class DECIMAL(BaseType):
    def __str__(self):
        return "DECIMAL"


class DOUBLE(BaseType):
    def __str__(self):
        return "DOUBLE"


class DURATION(BaseType):
    def __str__(self):
        return "DURATION"


class FLOAT(BaseType):
    def __str__(self):
        return "FLOAT"


class INET(BaseType):
    def __str__(self):
        return "INET"


class INT(BaseType):
    def __str__(self):
        return "INT"


class SMALLINT(BaseType):
    def __str__(self):
        return "SMALLINT"


class TEXT(BaseType):
    def __str__(self):
        return "TEXT"


class TIME(BaseType):
    def __str__(self):
        return "TIME"


class TIMESTAMP(BaseType):
    def __str__(self):
        return "TIMESTAMP"


class TIMEUUID(BaseType):
    def __str__(self):
        return "TIMEUUID"


class TINYINT(BaseType):
    def __str__(self):
        return "TINYINT"


class UUID(BaseType):
    def __str__(self):
        return "UUID"


class VARCHAR(BaseType):
    def __str__(self):
        return "VARCHAR"


class VARINT(BaseType):
    def __str__(self):
        return "VARINT"


class LIST(BaseType):
    type: BaseType

    def __init__(self, temp: BaseType):
        self.type = temp

    def __str__(self):
        return f"LIST <{str(self.type)}>"


class SET(BaseType):
    type: BaseType

    def __init__(self, temp: BaseType):
        self.type = temp

    def __str__(self):
        return f"SET <{str(self.type)}>"


class MAP(BaseType):
    key_type: BaseType
    value_type: BaseType

    def __init__(self, key_type: BaseType, value_type: BaseType):
        self.key_type = key_type
        self.value_type = value_type

    def __str__(self):
        return f"MAP <{str(self.key_type)},{str(self.value_type)}>"

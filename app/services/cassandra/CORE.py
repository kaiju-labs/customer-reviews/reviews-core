from typing import Union, Dict, List, Tuple
from .CORE_type import BaseType


class Condition:
    condition_result: str

    def __init__(self, str_temp: str):
        self.condition_result = str(str_temp)

    def __and__(self, other):
        self.condition_result += " AND " + str(other)
        return self

    def __str__(self):
        return self.condition_result


class ColumnOperators:
    operand: str

    def __init__(self, operand):
        self.operand = operand

    def __add__(self, other):
        return f"{self.operand} + {str(other)}"

    def __eq__(self, other):
        return Condition(f"{self.operand} = {str(other)}")

    def __gt__(self, other):
        return Condition(f"{self.operand} > {str(other)}")

    def __ge__(self, other):
        return Condition(f"{self.operand} >= {str(other)}")

    def __le__(self, other):
        return Condition(f"{self.operand} <= {str(other)}")

    def __lt__(self, other):
        return Condition(f"{self.operand} < {str(other)}")

    def __str__(self):
        return self.operand


class Columns(ColumnOperators):
    type: str
    partition_key: bool
    clustering_key: bool
    order: str
    static: str
    name: str

    def __init__(self, name: str, type_column: Union[str, BaseType], partition_key=False, static=False,
                 clustering_key=False, order: str = None):
        super(Columns, self).__init__(name)
        self.type = str(type_column)
        self.name = name
        self.partition_key = partition_key
        self.clustering_key = clustering_key
        self.order = order
        if static:
            self.static = "STATIC"
        else:
            self.static = ""

    def return_name(self):
        return self.name

    def return_static(self):
        return self.static

    def return_column(self):
        return self.name, self.type, self.static

    def return_partition_key(self):
        if self.partition_key:
            return self.name
        return False

    def return_clustering_key(self):
        if self.clustering_key:
            return self.name
        return False

    def return_order(self):
        if self.order:
            if self.clustering_key:
                return self.name, self.order
        return False

    def __str__(self):
        return self.name


def in_(temp: Columns, temp2):
    if isinstance(temp, Columns):
        return Condition(f"{temp.return_name()} IN {temp2}")
    if isinstance(temp, Tuple):
        rez = temp[0].return_name()
        for i in temp[1:]:
            rez += "," + i.return_name()
        return Condition(f"({rez}) IN {temp2}")


def contains_(temp: Columns, temp2):
    return Condition(f"{temp.return_name()} CONTAINS {temp2}")


class Table:
    name_Table: str
    columns_str: List[Tuple[str, str, str]]
    primary_key: Dict[str, list]
    order_by: List[Tuple[str, str]]
    _if_not_exists: bool

    def __init__(self, temp: Union[str, object] = None, *kwargs):
        self._if_not_exists = False
        if isinstance(temp, str):
            self.name_Table = temp
            self.primary_key = {"clustering_key": list(), "partition_key": list()}
            self.order_by = []
            self.columns_str = []
            for i in kwargs:
                self.__dict__[i.return_name()] = i
                self.columns_str.append(i.return_column())
                if i.return_clustering_key():
                    self.primary_key["clustering_key"].append(i.return_clustering_key())
                if i.return_partition_key():
                    self.primary_key["partition_key"].append(i.return_partition_key())
                if i.return_order():
                    self.order_by.append(i.return_order())

        else:
            self.name_Table = temp.__name__
            self.primary_key = {"clustering_key": list(), "partition_key": list()}
            self.order_by = []
            self.columns_str = []

            for k, i in vars(temp).items():
                if isinstance(i, Columns):
                    self.__dict__[i.return_name()] = i
                    self.columns_str.append(i.return_column())
                    if i.return_clustering_key():
                        self.primary_key["clustering_key"].append(i.return_clustering_key())
                    if i.return_partition_key():
                        self.primary_key["partition_key"].append(i.return_partition_key())
                    if i.return_order():
                        self.order_by.append(i.return_order())

    def compile(self):
        temp_columns = ", ".join(map(lambda x: f"{x[0]} {x[1]} {x[2]}", self.columns_str))
        if self.order_by:
            temp_order = ", ".join(map(lambda x: f"{x[0]} {x[1]}", self.order_by))
            temp_order = f"WITH CLUSTERING ORDER BY ({temp_order})"
        else:
            temp_order = ""

        temp_partition_key = ", ".join(self.primary_key["partition_key"])
        if self.primary_key["clustering_key"]:
            temp_partition_key = "(" + temp_partition_key + ")"
            temp_clustering_key = ", ".join(self.primary_key["clustering_key"])
            temp_clustering_key = ", " + temp_clustering_key
        else:
            temp_clustering_key = ""
        if self._if_not_exists:
            temp_exists = "iF NOT EXISTS "
        else:
            temp_exists = ""
        result = f"""
            CREATE TABLE {temp_exists} {self.name_Table}
            (
                {temp_columns},
                PRIMARY KEY ({temp_partition_key}{temp_clustering_key})
            ){temp_order};
        """
        return result

    def if_not_exists(self):
        self._if_not_exists = True
        return self

    def insert(self):
        return Insert(self)

    def select(self, *selector_temp):
        return Select(self, *selector_temp)

    def delete(self):
        return Delete(self)

    def update(self):
        return Update(self)


def and_(*agr):
    agr_temp = map(lambda x: str(x), agr)
    return " AND ".join(agr_temp)


def desc(col):
    return f"{col.return_name()} DESC"


def asc(col):
    return f"{col.return_name()} ASC"


def tuple_(*columns: Columns):
    return ColumnOperators(f"( {' , '.join(map(lambda x: str(x), columns))}  )")


class Select:
    """
    select_statement ::=  SELECT [ DISTINCT ] ( select_clause | '*' )
                      FROM table_name
                      [ WHERE where_clause ]
                      [ ORDER BY ordering_clause ]
                      [ PER PARTITION LIMIT (integer | bind_marker) ]
                      [ LIMIT (integer | bind_marker) ]
                      [ ALLOW FILTERING ]
                      [ BYPASS CACHE ]
                      [ USING TIMEOUT timeout ]
    select_clause    ::=  selector [ AS identifier ] ( ',' selector [ AS identifier ] )*
    selector         ::=  column_name
                          | CAST '(' selector AS cql_type ')'
                          | function_name '(' [ selector ( ',' selector )* ] ')'
                          | COUNT '(' '*' ')'
    where_clause     ::=  relation ( AND relation )*
    relation         ::=  column_name operator term
                          '(' column_name ( ',' column_name )* ')' operator tuple_literal
                          TOKEN '(' column_name ( ',' column_name )* ')' operator term
    operator         ::=  '=' | '<' | '>' | '<=' | '>=' | IN | CONTAINS | CONTAINS KEY
    ordering_clause  ::=  column_name [ ASC | DESC ] ( ',' column_name [ ASC | DESC ] )*
    timeout          ::=  duration
    """
    _timeout: Union[str, None]
    result: str
    selector: str
    where_condition: Union[Condition, None]
    order_list: list
    limit_int: Union[int, None]
    _allow_filtering: bool
    _bypass_cache: bool

    def __init__(self, table, *selector_temp):
        self.order_list = []
        self.limit_int = None
        self.where_condition = None
        self._allow_filtering = False
        self._bypass_cache = False
        self._timeout = None
        if selector_temp:
            self.selector = " , ".join(map(lambda x: str(x), selector_temp))
        else:
            self.selector = "*"
        self.result = f"SELECT {self.selector} FROM {table.name_Table}"

    def where(self, where_str):
        if self.where_condition is None:
            self.where_condition = Condition(where_str)
        else:
            self.where_condition &= where_str
        return self

    def order_by(self, *order_str):
        if len(order_str) == 1:
            self.order_list.append(*order_str)
        if len(order_str) > 1:
            self.order_list.extend(order_str)
        return self

    def allow_filtering(self):
        self._allow_filtering = True
        return self

    def bypass_cache(self):
        self._bypass_cache = True
        return self

    def limit(self, limit):
        self.limit_int = limit
        return self

    def timeout(self, duration):
        self._timeout = duration
        return self

    def __str__(self):
        if self.where_condition is not None:
            self.result += " WHERE " + str(self.where_condition)
        if self.order_list:
            self.result += " ORDER BY " + " , ".join(self.order_list)
        if self.limit_int is not None:
            self.result += " LIMIT " + str(self.limit_int)
        if self._allow_filtering:
            self.result += " ALLOW FILTERING "
        if self._bypass_cache:
            self.result += " BYPASS CACHE "
        if self._timeout is not None:
            self.result += " USING TIMEOUT " + str(self.limit_int)
        return self.result


class Delete:
    """
    delete_statement: DELETE [ `simple_selection` ( ',' `simple_selection` ) ]
                : FROM `table_name`
                : [ USING `update_parameter` ( AND `update_parameter` )* ]
                : WHERE `where_clause`
                : [ IF ( EXISTS | `condition` ( AND `condition` )* ) ]
    """
    result: str
    where_condition: Union[Condition, None]
    _timestamp: Union[int, None]
    _ttl: Union[int, None]
    _timeout: Union[str, None]

    def __init__(self, table):
        self.where_condition = None
        self.result = f"DELETE FROM {table.name_Table}"
        self._timestamp = None
        self._ttl = None
        self._timeout = None

    def timestamp(self, temp: int):
        self._timestamp = temp
        return self

    def ttl(self, temp: int):
        self._ttl = temp
        return self

    def timeout(self, duration):
        self._timeout = duration
        return self

    def where(self, where_str):
        if self.where_condition is None:
            self.where_condition = Condition(where_str)
        else:
            self.where_condition &= where_str
        return self

    def __str__(self):
        strand = ""
        if self._timeout is not None or self._timestamp is not None or self._ttl is not None:
            self.result += " USING "
        if self._timeout is not None:
            self.result += f"TIMEOUT {self._timeout}"
            strand = " AND "
        if self._timestamp is not None:
            self.result += strand
            self.result += f"TIMESTAMP {self._timestamp}"
            strand = " AND "
        if self._ttl is not None:
            self.result += strand
            self.result += f"TIMEOUT {self._ttl}"

        if self.where_condition != "":
            self.result += " WHERE " + str(self.where_condition)

        return self.result


class Update:
    """
    update_statement: UPDATE `table_name`
                : [ USING `update_parameter` ( AND `update_parameter` )* ]
                : SET `assignment` ( ',' `assignment` )*
                : WHERE `where_clause`
                : [ IF ( EXISTS | `condition` ( AND `condition` )*) ]
    update_parameter: ( TIMESTAMP `int_value` | TTL  `int_value` | TIMEOUT `duration` )
    int_value: ( `integer` | `bind_marker` )3
    assignment: `simple_selection` '=' `term`
              : | `column_name` '=' `column_name` ( '+' | '-' ) `term`
              : | `column_name` '=' `list_literal` '+' `column_name`
    simple_selection: `column_name`
                    : | `column_name` '[' `term` ']'
                    : | `column_name` '.' `field_name`
    condition: `simple_selection` `operator` `term`
    """
    result: str
    where_condition: Union[Condition, None]
    set_str: str
    _timestamp: Union[int, None]
    _ttl: Union[int, None]
    _timeout: Union[str, None]
    _if_conditional: Union[str, None]

    def __init__(self, table):
        self._if_conditional = None
        self.where_condition = None
        self.set_str = ""
        self.result = f"UPDATE {table.name_Table}"
        self._timestamp = None
        self._ttl = None
        self._timeout = None

    def where(self, where_str):
        if self.where_condition is None:
            self.where_condition = Condition(where_str)
        else:
            self.where_condition &= where_str
        return self

    def set(self, set_values: List):
        for i in set_values:
            if self.set_str != "":
                self.set_str += ", "
            self.set_str += f"{i[0].name} = {i[1]}"
        return self

    def timestamp(self, temp: int):
        self._timestamp = temp
        return self

    def ttl(self, temp: int):
        self._ttl = temp
        return self

    def timeout(self, duration):
        self._timeout = duration
        return self

    def _if(self, exist: bool = False, conditional: Condition = None):
        if exist:
            self._if_conditional = "EXIST"
        elif Condition is not None:
            self._if_conditional += str(conditional)

    def __str__(self) -> str:
        strand = ""
        if self._timeout is not None or self._timestamp is not None or self._ttl is not None:
            self.result += " USING "
        if self._timeout is not None:
            self.result += f"TIMEOUT {self._timeout}"
            strand = " AND "
        if self._timestamp is not None:
            self.result += strand
            self.result += f"TIMESTAMP {self._timestamp}"
            strand = " AND "
        if self._ttl is not None:
            self.result += strand
            self.result += f"TIMEOUT {self._ttl}"

        if self.set_str != "":
            self.result += " SET " + self.set_str
        if self.where_condition is not None:
            self.result += " WHERE " + str(self.where_condition)
        if self._if_conditional is not None:
            self.result += " IF " + str(self._if_conditional)
        return self.result


class Insert:
    """
    insert_statement:  INSERT INTO table_name ( `names_values` | `json_clause` )
                :  [ IF NOT EXISTS ]
                :  [ USING `update_parameter` ( AND `update_parameter` )* ];
    names_values: `names` VALUES `tuple_literal`
    json_clause:  JSON `string` [ DEFAULT ( NULL | UNSET ) ]
    names: '(' `column_name` ( ',' `column_name` )* ')'
    update_parameter: ( TIMESTAMP `int_value` | TTL  `int_value` | TIMEOUT `duration` )
    int_value: ( `integer` | `bind_marker` )
    """
    result: str
    columns: Dict[str, str]
    _timestamp: Union[int, None]
    _ttl: Union[int, None]
    _timeout: Union[str, None]
    if_not_exists: bool

    def __init__(self, table: Table, if_not_exists: bool = False):
        self.result = f"INSERT INTO {table.name_Table}"
        self.columns = {}
        self.if_not_exists = if_not_exists
        self._timestamp = None
        self._ttl = None
        self._timeout = None
        for i in table.columns_str:
            self.columns[i[0]] = "?"

    def value(self, column: Columns, value):
        self.columns[column.return_name()] = value
        return self

    def timestamp(self, temp: int):
        self._timestamp = temp
        return self

    def ttl(self, temp: int):
        self._ttl = temp
        return self

    def timeout(self, duration):
        self._timeout = duration
        return self

    def __str__(self):
        temp_columns = ",".join(map(lambda x: f"{x}", self.columns.keys()))
        temp_columns_values = ",".join(map(lambda x: str(x), self.columns.values()))
        self.result += f" ({temp_columns}) VALUES ({temp_columns_values})"""
        if self.if_not_exists:
            self.result += " IF NOT EXISTS "
        strand = ""
        if self._timeout is not None or self._timestamp is not None or self._ttl is not None:
            self.result += " USING "
        if self._timeout is not None:
            self.result += f"TIMEOUT {self._timeout}"
            strand = " AND "
        if self._timestamp is not None:
            self.result += strand
            self.result += f"TIMESTAMP {self._timestamp}"
            strand = " AND "
        if self._ttl is not None:
            self.result += strand
            self.result += f"TIMEOUT {self._ttl}"
        return self.result

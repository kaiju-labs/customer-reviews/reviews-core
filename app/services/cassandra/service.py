import asyncio
import time

import acsylla

from kaiju_tools.services import ContextableService


class ScyllaService(ContextableService):
    service_name = 'scylla'
    keyspace: str = None
    cluster: acsylla.Cluster = None
    logger = None

    def __init__(self, app, keyspace, logger=None, **kwargs):
        """
        :param app:
        :param keyspace:
        :param kwargs: {
            contact_points: List[str],
            port: int = 9042,
            username: str = None,
            password: str = None,
            protocol_version: int = 3,
            connect_timeout: float = 5.0,
            request_timeout: float = 2.0,
            resolve_timeout: float = 1.0,
            consistency: Consistency = Consistency.LOCAL_ONE,
            core_connections_per_host: int = 1,
            local_port_range_min: int = 49152,
            local_port_range_max: int = 65535,
            application_name: str = "acsylla",
            application_version: str = __version__,
            num_threads_io: int = 1,
            ssl_enabled: bool = False,
            ssl_cert: str = None,
            ssl_private_key: str = None,
            ssl_private_key_password: str = "",
            ssl_trusted_cert: str = None,
            ssl_verify_flags: SSLVerifyFlags = SSLVerifyFlags.PEER_CERT,
        }
        """
        self.conf = kwargs
        super().__init__(app=app, logger=logger)
        self.keyspace = keyspace
        self.session = None
        self.logger = logger
        self.flag = True

    async def init(self):

        self.cluster = acsylla.create_cluster(**self.conf)
        await self.create_keyspace()
        if self.flag:
            self.session = ScyllaSession(self.app, self.flag, keyspace=self.keyspace)
            await self.session.init()

    async def close(self):
        if self.session is not None:
            await self.session.close()

    async def create_keyspace(self):
        """Создание keyspace"""
        self.logger.info(self.conf["contact_points"])
        ses = await self.cluster.create_session()
        statement = acsylla.create_statement(
            f"CREATE KEYSPACE IF NOT EXISTS  {self.keyspace} WITH replication = {{'class': "
            f"'SimpleStrategy', 'replication_factor' : 1}};")
        await ses.execute(statement)
        await ses.close()

    @property
    def db(self):
        if self.flag:
            return self.session
        else:
            return ScyllaSession(self.app, self.flag, keyspace=self.keyspace)


class ScyllaSession:
    session: acsylla.Session = None

    def __init__(self, app, flag, keyspace=None):
        self.app = app
        self.keyspace = keyspace
        self.flag = flag

    async def init(self):

        self.session = await self.app.services.scylla.cluster.create_session(
            keyspace=self.keyspace or self.app.services.scylla.keyspace)

    async def close(self):
        await self.session.close()

    async def create_prepared(self, obj):
        return await self.session.create_prepared(str(obj))

    async def __aenter__(self):
        if self.flag:
            return self
        else:
            self.session = await self.app.services.scylla.cluster.create_session(
                keyspace=self.keyspace or self.app.services.scylla.keyspace)

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if not self.flag:
            await self.session.close()

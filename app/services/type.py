from uuid import UUID
from typing import TypedDict, List, Optional
from datetime import datetime


class MessageInfo(TypedDict):
    message_id: UUID
    bucket: int


class AttachmentInfo(TypedDict):
    attachment: UUID
    tags: List[str]


class MessageStatus(TypedDict):
    status: int
    list_user_who_read: Optional[List[UUID]]


class Message(TypedDict):
    message_info: MessageInfo
    chat_id: UUID
    owner_id: UUID
    status: MessageStatus
    count_in_bucket: int
    attachment: Optional[List[UUID]]
    data: str
    created_time: datetime
    updated_time: Optional[datetime]


class MessageListData(TypedDict):
    message_list: List[Message]


class ReturnMessageList(TypedDict):
    data: MessageListData
    status: str


class Attachment(TypedDict):
    attachment_id: UUID
    tags: List[str]
    file: str
    message_id: UUID
    chat_id: UUID
    created_time: datetime


class ReturnAttachmentData(TypedDict):
    data: List[Attachment]
    status: str

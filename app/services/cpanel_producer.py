from kaiju_redis import RedisProducer, RedisTransportService
import datetime


class CpannelProducer(RedisProducer):
    async def init(self):
        self._transport = self.discover_service(
            self._transport, cls=RedisTransportService)
        await super().init()

    async def notify(self, correlation_id, review_id, company_id, parent, event):
        await self.send(namespace="cpannel_review_hub", topic="events", body={
            "type": "review",
            "id": (correlation_id, review_id, company_id, parent),
            "event": event,
            "timestamp": datetime.datetime.now(),
        })

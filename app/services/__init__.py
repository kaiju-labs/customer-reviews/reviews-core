"""
Import all your custom services here.
"""

from .cassandra.service import ScyllaService
from .review import ReviewService
from .redis_create_consumer import *
from .cpanel_producer import *

import asyncio
import uuid
import datetime
import time
import acsylla
from typing import Dict, Union, Tuple

from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc.abc import AbstractRPCCompatible
from kaiju_tools.encoding.json import dumps, loads

import app.models.tables as tables
from .cassandra.CORE import asc, and_, in_
# from .review_search import NoModeratorReview, ModeratorReview


# docker exec -it "/scylla-db" cqlsh
# DROP KEYSPACE kaiju_review_core;

class AsyncResultGenerator:
    def __init__(self, session, statement):
        self.session = session
        self.statement = statement

    async def __aiter__(self):
        result = await self.session.execute(self.statement)
        while True:
            if result.has_more_pages():
                self.statement.set_page_state(result.page_state())
                future_result = asyncio.create_task(
                    self.session.execute(self.statement))
                await asyncio.sleep(0)
            else:
                future_result = None
            for row in result:
                yield dict(row)
            if future_result is not None:
                result = await future_result
            else:
                break


class ReviewService(ContextableService, AbstractRPCCompatible):
    service_name = "review"
    logger = None

    @property
    def routes(self):
        return {
            # 'test': self.test,
            'create': self.create,
            'moderate_list_by_list': self.moderate_list_by_list,
            # 'moderation_update': self.moderation_update,
            "re_moderation": self.re_moderation_all,
            'mark_as_moderated': self.mark_as_moderated,
            'save_status_moderator': self.save_status_moderator,
            'moderate_list': self.moderate_list,
            'list': self.list,
            'list_cpannel': self.list_cpannel,
            'export_all': self.export_all,
            'all_moderator_ok': self.all_moderator_ok,
            'return_to_moderate': self.return_to_moderate,
            'add_moderation': self.add_to_moderate_stream,
            'export_by_dict': self.export_by_dict,
            'list_children': self.list_children,
            'list_by_customer': self.list_by_customer,
            'list_rating_and_average': self.list_rating_and_average,
            'delete': self.delete

        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    def __init__(self, logger=None, *args, amount_child_reviews=10, **kwargs):
        super().__init__(*args, **kwargs)
        self.prepared_statement = dict()
        self.amount_child_reviews = amount_child_reviews
        self.logger = logger

    async def create_prepared_list_statement(self):
        async with self.app.services.scylla.db as db:
            self.prepared_statement["moderating_list_company_id_and_review_id"] = await db.create_prepared(
                tables.moderating.select(tables.moderating.company_id, tables.moderating.review_id))
            self.prepared_statement["review_list_newest"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                )))
            self.prepared_statement["review_list"] = await db.create_prepared(
                tables.reviews.select())
            self.prepared_statement["review_list_by_list"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                    in_(tables.reviews.review_id, "?")
                )))
            self.prepared_statement["review_list_by_list_and_list"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    in_(tables.reviews.correlation_id, "?"),
                    tables.reviews.parent == "''",
                    in_(tables.reviews.review_id, "?")
                )))
            self.prepared_statement["review_list_by_list_and_list_and_list"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    in_(tables.reviews.correlation_id, "?"),
                    in_(tables.reviews.parent, "?"),
                    in_(tables.reviews.review_id, "?")
                )))
            self.prepared_statement["moderate_list_by_list_and_list"] = await db.create_prepared(
                tables.moderating.select().where(and_(
                    in_(tables.moderating.company_id, "?"),
                    in_(tables.moderating.review_id, "?")
                )))
            self.prepared_statement["review_list_by_list_children"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    in_(tables.reviews.parent, "?"),
                    in_(tables.reviews.review_id, "?")
                )))
            self.prepared_statement["review_list_before"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                    tables.reviews.review_id > "?"
                )).order_by(asc(tables.reviews.parent), asc(tables.reviews.review_id)))
            self.prepared_statement["review_list_after"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                    tables.reviews.review_id <= "?"
                )))
            self.prepared_statement["customer_list"] = await db.create_prepared(
                tables.customer_reviews.select().where(
                    and_(
                        tables.customer_reviews.customer == "?",
                        tables.customer_reviews.review_id < "?",
                    )))
            self.prepared_statement["moderate_list"] = await db.create_prepared(
                tables.moderating.select())
            self.prepared_statement["moderate_list_by_time"] = await db.create_prepared(
                tables.moderating.select().where(and_(
                    tables.moderating.company_id == "?",
                    tables.moderating.review_id < "?",
                )))
            self.prepared_statement["review_list_after_children"] = await db.create_prepared(
                tables.reviews.select().where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "?",
                    tables.reviews.review_id < "?"
                )))
            self.prepared_statement["review_list_by_customer"] = await db.create_prepared(
                tables.customer_reviews.select().where(and_(
                    tables.customer_reviews.customer == "?",
                    tables.customer_reviews.review_id < "?",
                )))
            self.prepared_statement["rating_list"] = await db.create_prepared(
                tables.ratings.select().where(tables.ratings.correlation_id == "?"))
            self.prepared_statement["rating_list_by_list"] = await db.create_prepared(
                tables.ratings.select().where(in_(tables.ratings.correlation_id, "?")))

    async def create_prepared_insert_statement(self):
        async with self.app.services.scylla.db as db:
            self.prepared_statement["reviews_insert"] = await db.create_prepared(tables.reviews.insert())
            self.prepared_statement["customer_reviews_insert"] = await db.create_prepared(
                tables.customer_reviews.insert())
            self.prepared_statement["moderating_reviews_insert"] = await db.create_prepared(tables.moderating.insert())

    async def create_prepared_delete_statement(self):
        async with self.app.services.scylla.db as db:
            self.prepared_statement["customer_delete"] = await db.create_prepared(
                tables.customer_reviews.delete().where(
                    and_(
                        tables.customer_reviews.customer == "?",
                        tables.customer_reviews.review_id == "?",
                    )))
            self.prepared_statement["review_delete"] = await db.create_prepared(
                tables.reviews.delete().where(
                    and_(
                        tables.reviews.correlation_id == "?",
                        tables.reviews.parent == "?",
                        tables.reviews.review_id == "?"
                    )))
            self.prepared_statement["review_children_delete"] = await db.create_prepared(
                tables.reviews.delete().where(
                    and_(
                        tables.reviews.correlation_id == "?",
                        tables.reviews.parent == "?"
                    )))
            self.prepared_statement["moderate_delete"] = await db.create_prepared(
                tables.moderating.delete().where(
                    and_(
                        tables.moderating.company_id == "?",
                        tables.moderating.review_id == "?"
                    )))

    async def list_cpannel(self, list_id):
        """
        list_id = [(correlation_id, review_id, company_id, parent)]
        """
        correlation_id_list = []
        review_id_list = []
        company_id_list = []
        parent_list = []
        for correlation_id, review_id, company_id, parent in list_id:
            correlation_id_list.append(correlation_id)
            review_id_list.append(review_id)
            company_id_list.append(company_id)
            parent_list.append(parent)
        statement_select_review = self.prepared_statement["review_list_by_list_and_list_and_list"].bind(
        )
        statement_select_review.bind_list(
            [correlation_id_list, parent_list, review_id_list])
        select_moderate_list = self.prepared_statement["moderate_list_by_list_and_list"].bind(
        )
        select_moderate_list.bind_list([company_id_list, review_id_list])
        response = []
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(select_moderate_list)
            for i in result:
                response.append(i.as_dict())
            result = await db.session.execute(statement_select_review)
            for i in result:
                response.append(i.as_dict())
        return response

    async def create_prepared_update_statement(self):
        async with self.app.services.scylla.db as db:
            self.prepared_statement["moderate_update_data"] = await db.create_prepared(
                tables.moderating.update().set([(tables.moderating.data, '?'),
                                                (tables.moderating.meta, '?'),
                                                (tables.moderating.rating, '?'),
                                                (tables.moderating.updated, '?')
                                                ])
                .where(and_(
                    tables.moderating.review_id == "?",
                    tables.moderating.company_id == "?"
                )))
            self.prepared_statement["moderate_update_moderator_status"] = await db.create_prepared(
                tables.moderating.update().set(
                    [(tables.moderating.moderator_status, '?')])
                .where(and_(
                    tables.moderating.review_id == "?",
                    tables.moderating.company_id == "?"
                )))
            self.prepared_statement["review_update_children"] = await db.create_prepared(
                tables.reviews.update().set([(tables.reviews.children, '?')]).where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "?",
                    tables.reviews.review_id == "?"
                )))
            self.prepared_statement["customer_reviews_update_moderate"] = await db.create_prepared(
                tables.customer_reviews.update().set([(tables.customer_reviews.is_moderated, '?')]).where(and_(
                    tables.customer_reviews.customer == "?",
                    tables.customer_reviews.review_id == "?"
                )))
            self.prepared_statement["ratings_update"] = await db.create_prepared(tables.ratings.update().set(
                [(tables.ratings.value, 'value + 1')]).where(and_(
                    tables.ratings.correlation_id == "?",
                    tables.ratings.rating == "?"
                )))

    async def create_prepared_statement(self):
        async with self.app.services.scylla.db as db:
            # ___________________________MATH_________________________________________
            self.prepared_statement["review_count_after"] = await db.create_prepared(
                tables.reviews.select("COUNT(*)").where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                    tables.reviews.review_id <= "?"
                )))
            self.prepared_statement["review_count_before"] = await db.create_prepared(
                tables.reviews.select("COUNT(*)").where(and_(
                    tables.reviews.correlation_id == "?",
                    tables.reviews.parent == "''",
                    tables.reviews.review_id > "?"
                )))

    async def init(self):
        async with self.app.services.scylla.db as db:
            await db.session.execute(acsylla.create_statement(tables.reviews.if_not_exists().compile()))
            await db.session.execute(acsylla.create_statement(tables.moderating.if_not_exists().compile()))
            await db.session.execute(acsylla.create_statement(tables.customer_reviews.if_not_exists().compile()))
            await db.session.execute(acsylla.create_statement(tables.ratings.if_not_exists().compile()))
        await db.session.execute(acsylla.create_statement("""
            CREATE INDEX IF NOT EXISTS ON reviews((correlation_id),review_id);
        """))
        await self.create_prepared_statement()
        await self.create_prepared_list_statement()
        await self.create_prepared_insert_statement()
        await self.create_prepared_update_statement()
        await self.create_prepared_delete_statement()

    async def create(self, customer, data, meta, correlation_id, company_id, rating=5, status=0, parent=""):
        async with self.app.services.scylla.db as db:
            statement_insert_review = self.prepared_statement["moderating_reviews_insert"].bind(
            )
            statement_insert_customer = self.prepared_statement["customer_reviews_insert"].bind(
            )
            review_id = uuid.uuid1()
            statement_insert_review.bind_dict({
                "review_id": review_id,
                "correlation_id": correlation_id,
                "data": dumps(data),
                "updated": None,
                "status": status,
                "rating": rating,
                "meta": dumps(meta),
                "customer": customer,
                "parent": str(parent),
                "company_id": company_id
            })
            statement_insert_customer.bind_dict({
                "review_id": review_id,
                "correlation_id": correlation_id,
                "customer": customer,
                "company_id": company_id,
                "parent": str(parent),
                "is_moderated": False
            })
            await db.session.execute(statement_insert_review)
            await db.session.execute(statement_insert_customer)
            await self.add_to_moderate_stream(review_id, company_id)
            await self.app.services.producer_cpannel.notify(correlation_id, review_id, company_id, parent, "create")
            return {
                "review_id": review_id
            }

    async def list(self, correlation_id, limit=10, paging_id=None):
        response = {
            "paging_dict": {
            },
            "review_list": {
            },
            "current_page": 1
        }
        start_time = time.time()
        children_list = []
        parent_list = []
        async with self.app.services.scylla.db as db:
            if paging_id is None:
                statement_select = self.prepared_statement["review_list_newest"].bind(
                    page_size=limit * 5)
                statement_select.bind_dict({
                    "correlation_id": correlation_id
                })
                result = await db.session.execute(statement_select)
                count = 0
                children_list = []
                for i in result:
                    review = i.as_dict()
                    if count % limit == 0:
                        response['paging_dict'][str(
                            (count // limit) + 1)] = review['review_id']
                    if count < limit:
                        response['review_list'][str(
                            review['review_id'])] = return_review_type(review)
                        if review['children'] is not None and len(review['children']) > 0:
                            children_list.extend(review['children'])
                            parent_list.append(review['review_id'])
                    count += 1

            else:
                select_before_count = self.prepared_statement["review_count_before"].bind(
                )
                select_before_count.bind_dict({
                    "correlation_id": correlation_id,
                    "review_id": paging_id
                })
                select_after_count = self.prepared_statement["review_count_after"].bind(
                )
                select_after_count.bind_dict({
                    "correlation_id": correlation_id,
                    "review_id": paging_id
                })
                limit_before = limit * 2
                limit_after = limit * 3
                result_count_before = await db.session.execute(select_before_count)
                result_count_after = await db.session.execute(select_after_count)
                result_before_count = result_count_before.first().as_dict()[
                    'count']
                result_after_count = result_count_after.first().as_dict()[
                    'count']
                current_page = result_before_count // limit + 1
                response['current_page'] = current_page
                if result_before_count < limit_before:
                    limit_after += limit_before - result_before_count
                if result_after_count < limit_after:
                    limit_before += limit_after - result_after_count
                self.logger.debug("LIMITS before %s after %s",
                                  limit_before, limit_after)
                start_page = max(current_page - limit_before // limit, 1)
                select_list_before = self.prepared_statement["review_list_before"].bind(
                    page_size=limit_before)
                select_list_before.bind_dict({
                    "correlation_id": correlation_id,
                    "review_id": paging_id
                })
                select_list_after = self.prepared_statement["review_list_after"].bind(
                    page_size=limit_after)
                select_list_after.bind_dict({
                    "correlation_id": correlation_id,
                    "review_id": paging_id
                })
                temp_result_before = await db.session.execute(select_list_before)
                result_after = await db.session.execute(select_list_after)
                count = 0
                result_before = []
                for i in temp_result_before:
                    result_before.append(i.as_dict())
                result_before.reverse()
                for review in result_before:
                    if count % limit == 0:
                        response['paging_dict'][str(
                            (count // limit) + start_page)] = review['review_id']
                    if current_page == (count // limit) + start_page:
                        response['review_list'][str(
                            review['review_id'])] = return_review_type(review)
                        if review['children'] is not None and len(review['children']) > 0:
                            children_list.extend(review['children'])
                            parent_list.append(review['review_id'])
                    count += 1
                for i in result_after:
                    review = i.as_dict()
                    if count % limit == 0:
                        response['paging_dict'][str(
                            (count // limit) + start_page)] = review['review_id']
                    if current_page == (count // limit) + start_page:
                        response['review_list'][str(
                            review['review_id'])] = return_review_type(review)
                        if review['children'] is not None and len(review['children']) > 0:
                            children_list.extend(review['children'])
                            parent_list.append(review['review_id'])
                    count += 1
            if len(children_list) > 0:
                statement_select_children = self.prepared_statement["review_list_by_list_children"].bind(
                )
                statement_select_children.bind_list(
                    [correlation_id, parent_list, children_list])
                result_children = await db.session.execute(statement_select_children)
                for child in result_children:
                    child_review = child.as_dict()
                    response['review_list'][child_review['parent']]["children"].append(
                        return_review_type(child_review))
        stop_time = time.time()
        response['review_list'] = list(response['review_list'].values())
        self.logger.debug("method list time %s", stop_time - start_time)
        return response

    async def list_children(self, correlation_id, parent, paging_id, limit=10):
        response = {
            "paging_id": "",
            "review_list": {
            }
        }
        statement_select = self.prepared_statement["review_list_after_children"].bind(
            page_size=limit)
        statement_select.bind_dict({
            "correlation_id": correlation_id,
            "parent": parent,
            "review_id": paging_id
        })
        async with self.app.services.scylla.db as db:
            result = await db.execute(statement_select)
            for i in result:
                review = i.as_dict()
                response['review_list'][str(
                    review['review_id'])] = return_review_type(review)
                response['paging_id'] = str(review['review_id'])

        return response

    async def delete(self, correlation_id, customer, moderated, parent, review_id, company_id=''):
        if moderated:
            statement_delete_customer = self.prepared_statement["customer_delete"].bind(
            )
            statement_delete_customer.bind_dict({
                "customer": customer,
                "review_id": review_id,
            })
            statement_delete_review = self.prepared_statement["review_delete"].bind(
            )
            statement_delete_review.bind_dict({
                "correlation_id": correlation_id,
                "parent": parent,
                "review_id": review_id
            })
            statement_delete_review_children = self.prepared_statement["review_children_delete"].bind(
            )
            statement_delete_review_children.bind_dict({
                "correlation_id": correlation_id,
                "parent": parent
            })
            async with self.app.services.scylla.db as db:
                batch = acsylla.create_batch_logged()
                batch.add_statement(statement_delete_review)
                batch.add_statement(statement_delete_customer)
                batch.add_statement(statement_delete_review_children)
                await db.execute_batch(batch)
        else:
            async with self.app.services.scylla.db as db:
                statement_delete_moderate = self.prepared_statement["customer_moderate"].bind(
                )
                statement_delete_moderate.bind_dict({
                    "company_id": company_id,
                    "correlation_id": correlation_id
                })

                await db.session.execute(statement_delete_moderate)
        await self.app.services.producer_cpannel.notify(correlation_id, review_id, company_id, parent, "delete")
        return {"result": True}

    async def export_all(self, moderate_status, page=None):
        if moderate_status:
            statement = self.prepared_statement["review_list"]
        else:
            statement = self.prepared_statement["moderate_list"]
        execute_statement = statement.bind(page_size=300)
        if page is not None:
            execute_statement.statement.set_page_state(bytes(page))
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(execute_statement)
        response = []
        for i in result:
            review = i.as_dict()
            data_review = return_review_type(review)
            response.append(data_review)
        temp = None
        if result.has_more_pages():
            temp = result.page_state()
        return response, [x for x in temp]

    async def moderation_update(self, company_id, review_id, data, meta, rating, correlation_id, parent):
        statement_update = self.prepared_statement['moderate_update_data'].bind(
        )
        statement_update.bind_dict({
            "compony_id": company_id,
            "correlation_id": correlation_id,
            "parent": parent,
            "review_id": review_id,
            "updated": uuid.uuid1(),
            "data": data,
            "meta": meta,
            "rating": rating
        })

        async with self.app.services.scylla.db as db:
            result = await db.session.execute(statement_update)
        return {"result": True}

    async def mark_as_moderated_without_change(self, company_id, review_id):
        select_moderate_list = self.prepared_statement["moderate_list_by_list_and_list"].bind(
        )
        select_moderate_list.bind_list([[company_id], [review_id]])
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(select_moderate_list)
            for i in result:
                review = i.as_dict()
                data_review = return_review_type(review)
                del data_review['children']
                data_review['parent'] = review['parent']
                review_list = data_review
        statement_update_customer_review = self.prepared_statement["customer_reviews_update_moderate"].bind(
        )
        statement_delete_moderate = self.prepared_statement["moderate_delete"].bind(
        )
        statement_insert_rating = self.prepared_statement["ratings_update"].bind(
        )
        statement_insert_review = self.prepared_statement["reviews_insert"].bind(
        )
        statement_insert_review.bind_dict({
            "review_id": review_id,
            "correlation_id": review_list["correlation_id"],
            "data": dumps(review_list["data"]),
            "updated": None,
            "company_id": company_id,
            "status": review_list["status"],
            "rating": review_list["rating"],
            "meta": dumps(review_list["meta"]),
            "customer": review_list["customer"],
            "children": [],
            "parent": str(review_list["parent"])
        })
        statement_update_customer_review.bind_dict({
            "review_id": review_id,
            "customer": review_list["customer"],
            "is_moderated": True
        })
        statement_delete_moderate.bind_dict({
            "review_id": review_id,
            "company_id": company_id
        })
        statement_insert_rating.bind_dict({
            "correlation_id": review_list["correlation_id"],
            "rating": review_list["rating"]
        })
        if review_list["parent"] != '':
            statement_select = self.prepared_statement["review_list_by_list"].bind(
            )
            statement_select.bind_list(
                [review_list["correlation_id"], [review_list["parent"]]])
            statement_update_review = self.prepared_statement["review_update_children"].bind(
            )
            async with self.app.services.scylla.db as db:
                result_select = await db.session.execute(statement_select)
                parent_info = result_select.first().as_dict()
                children_list = parent_info['children']
                if children_list is None:
                    children_list = []
                sorted(children_list, key=lambda x: x[0].time, reverse=False)
                children_list.append(review_id)
                if len(children_list) > 5:
                    children_list = children_list[1:]
                statement_update_review.bind_dict({
                    "correlation_id": review_list["correlation_id"],
                    "parent": '',
                    "review_id": review_list["parent"],
                    'children': children_list
                })
                await db.session.execute(statement_update_review)
        async with self.app.services.scylla.db as db:
            await db.session.execute(statement_insert_review)
            await db.session.execute(statement_update_customer_review)
            await db.session.execute(statement_delete_moderate)
            await db.session.execute(statement_insert_rating)

        return {"Result": True}

    async def mark_as_moderated(self, company_id, review_id, customer, data, meta, rating, correlation_id, status=0,
                                parent=""):
        statement_update_customer_review = self.prepared_statement["customer_reviews_update_moderate"].bind(
        )
        statement_delete_moderate = self.prepared_statement["moderate_delete"].bind(
        )
        statement_insert_rating = self.prepared_statement["ratings_update"].bind(
        )
        statement_insert_review = self.prepared_statement["reviews_insert"].bind(
        )
        statement_insert_review.bind_dict({
            "review_id": review_id,
            "correlation_id": correlation_id,
            "data": dumps(data),
            "updated": None,
            "company_id": company_id,
            "status": status,
            "rating": rating,
            "meta": dumps(meta),
            "customer": customer,
            "children": [],
            "parent": str(parent)
        })
        statement_update_customer_review.bind_dict({
            "review_id": review_id,
            "customer": customer,
            "is_moderated": True
        })
        statement_delete_moderate.bind_dict({
            "review_id": review_id,
            "company_id": company_id
        })
        statement_insert_rating.bind_dict({
            "correlation_id": correlation_id,
            "rating": rating
        })
        if parent != '':
            statement_select = self.prepared_statement["review_list_by_list"].bind(
            )
            statement_select.bind_list([correlation_id, [parent]])
            statement_update_review = self.prepared_statement["review_update_children"].bind(
            )

            async with self.app.services.scylla.db as db:
                result_select = await db.session.execute(statement_select)
                parent_info = result_select.first().as_dict()
                children_list = parent_info['children']
                if children_list is None:
                    children_list = []
                sorted(children_list, key=lambda x: x[0].time, reverse=False)
                children_list.append(review_id)
                if len(children_list) > 5:
                    children_list = children_list[1:]
                statement_update_review.bind_dict({
                    "correlation_id": correlation_id,
                    "parent": '',
                    "review_id": parent,
                    'children': children_list
                })
                await db.session.execute(statement_update_review)
        await self.app.services.producer_cpannel.notify(correlation_id, review_id, company_id, parent, "update")
        async with self.app.services.scylla.db as db:
            await db.session.execute(statement_insert_review)
            await db.session.execute(statement_update_customer_review)
            await db.session.execute(statement_delete_moderate)
            await db.session.execute(statement_insert_rating)
        return {"Result": True}

    async def moderate_list_by_list(self, list_review):
        review_list = []
        list_company_id = []
        list_review_id = []
        for i in list_review:
            list_company_id.append(i["company_id"])
            list_review_id.append(i["review_id"])
        select_moderate_list = self.prepared_statement["moderate_list_by_list_and_list"].bind(
        )
        select_moderate_list.bind_list([list_company_id, list_review_id])
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(select_moderate_list)
            for i in result:
                review = i.as_dict()
                data_review = return_review_type(review)
                del data_review['children']
                data_review['parent'] = review['parent']
                review_list.append(data_review)
        return review_list

    async def moderate_list(self, company_id, limit=10, paging_id=None):
        response = {
            "paging_id": "",
            "review_list": []
        }
        if paging_id is None:
            paging_id = uuid.uuid1()
        statement_select = self.prepared_statement["moderate_list_by_time"].bind(
            page_size=limit)
        statement_select.bind_dict(
            {"company_id": company_id, "review_id": paging_id})
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(statement_select)
            for i in result:
                review = i.as_dict()
                data_review = return_review_type(review)
                del data_review['children']
                data_review['parent'] = review['parent']
                data_review['moderator_status'] = review['moderator_status']
                response["review_list"].append(data_review)
                if response['paging_id'] == "":
                    response['paging_id'] = review['review_id']
                if response['paging_id'].int > review['review_id'].int:
                    response['paging_id'] = review['review_id']
        return response

    async def list_by_customer(self, customer, paging_id=None, limit=10):
        if paging_id is None:
            paging_id = uuid.uuid1()
        select_customer_review = self.prepared_statement["review_list_by_customer"].bind(
            page_size=limit)
        select_customer_review.bind_dict({
            "customer": customer,
            "review_id": paging_id
        })
        list_moderate = {"company_id": [], "review_id": []}
        list_review = {"correlation_id": [], "review_id": [], "parent": []}
        response = {
            "paging_id": "",
            "review_list": {
            }
        }
        async with self.app.services.scylla.db as db:
            result = await db.session.execute(select_customer_review)
            for i in result:
                row = i.as_dict()
                if row['is_moderated']:
                    list_review["correlation_id"].append(row['correlation_id'])
                    list_review["parent"].append(row['parent'])
                    list_review["review_id"].append(row['review_id'])
                else:
                    list_moderate["company_id"].append(row['company_id'])
                    list_moderate["review_id"].append(row['review_id'])
                if response['paging_id'] == "":
                    response['paging_id'] = row['review_id']
                if response['paging_id'].int > row['review_id'].int:
                    response['paging_id'] = row['review_id']
            select_review_list = self.prepared_statement["review_list_by_list_and_list_and_list"].bind(
            )
            select_review_list.bind_list(
                [list_review["correlation_id"], list_review["parent"], list_review["review_id"]])
            review_result = await db.session.execute(select_review_list)
            for i in review_result:
                review = i.as_dict()
                response['review_list'][str(
                    review['review_id'])] = return_review_type(review)
            select_moderate_list = self.prepared_statement["moderate_list_by_list_and_list"].bind(
            )
            select_moderate_list.bind_list(
                [list_moderate["company_id"], list_moderate["review_id"]])
            moderate_result = await db.session.execute(select_moderate_list)
            for i in moderate_result:
                review = i.as_dict()
                response['review_list'][str(
                    review['review_id'])] = return_review_type(review)
            response['review_list'] = list(response['review_list'].values())
            return response

    async def re_moderation_all(self):
        async with self.app.services.scylla.db as db:
            statement = self.prepared_statement["moderating_list_company_id_and_review_id"]
            execute_statement = statement.bind(page_size=300)
            async for row in AsyncResultGenerator(db.session, execute_statement):
                row = dict(row)
                await self.add_to_moderate_stream(str(row['review_id']), row['company_id'])

    async def list_rating_and_average(self, correlation_id):
        response = []
        if isinstance(correlation_id, uuid.UUID):
            correlation_id = str(correlation_id)
        if isinstance(correlation_id, str):
            correlation_id = [correlation_id]
        if isinstance(correlation_id, list):
            list_correlation = []
            for i in correlation_id:
                if isinstance(i, uuid.UUID):
                    i = str(correlation_id)
                list_correlation.append(i)
            statement_ratings = self.prepared_statement["rating_list_by_list"].bind(
            )
            statement_ratings.bind_list([list_correlation])
            average = {}
            async with self.app.services.scylla.db as db:
                result_rating = await db.session.execute(statement_ratings)
                for item in result_rating:
                    rate = item.as_dict()
                    if rate['correlation_id'] in average:
                        average[rate['correlation_id']
                                ]["count"] += rate["value"]
                        average[rate['correlation_id']
                                ]["average"] += rate["value"] * rate["rating"]
                        average[rate['correlation_id']]["rating"].append(
                            {"rating": rate['rating'], "value": rate['value']})
                    else:
                        average[rate['correlation_id']] = {}
                        average[rate['correlation_id']
                                ]["count"] = rate["value"]
                        average[rate['correlation_id']
                                ]["average"] = rate["value"] * rate["rating"]
                        average[rate['correlation_id']]["rating"] = [
                            {"rating": rate['rating'], "value": rate['value']}]

                for key, value in average.items():
                    response.append({"correlation_id": key,
                                     "ratings": value["rating"],
                                     "count": value['count'],
                                     "average": value["average"] / value["count"]
                                     })
        return response

    async def return_to_moderate(self, parent, review_id, correlation_id):
        pass
    #     statement_select_review = self.prepared_statement["review_list_by_list_and_list_and_list"].bind()
    #     statement_select_review.bind_list([[correlation_id], [parent], [review_id]])
    #     async with self.app.services.scylla.db as db:
    #         review = await db.session.execute(statement_select_review)
    #     statement_insert_moderate = self.prepared_statement["moderating_reviews_insert"].bind()

    async def save_status_moderator(self, review_id, company_id, moderators: Dict[str, bool]):
        statement_update_review = self.prepared_statement["moderate_update_moderator_status"].bind(
        )
        statement_update_review.bind_list(
            [dumps(moderators), review_id, company_id])
        async with self.app.services.scylla.db as db:
            await db.session.execute(statement_update_review)

    async def all_moderator_ok(self, review_id, company_id):
        await self.app.services.review.mark_as_moderated_without_change(company_id, review_id)

    async def create_batch(self, list_for_create):
        batch = acsylla.create_batch_unlogged()
        list_to_moderate = []
        list_to_cpannel = []
        for item in list_for_create:
            statement_insert_review = self.prepared_statement["moderating_reviews_insert"].bind(
            )
            statement_insert_customer = self.prepared_statement["customer_reviews_insert"].bind(
            )
            review_id = uuid.uuid1()
            list_to_moderate.append((review_id, item['company_id']))
            list_to_cpannel.append(
                (item['correlation_id'], review_id, item['company_id'], str(item.get('parent', ''))))
            statement_insert_review.bind_dict({
                "review_id": review_id,
                "correlation_id": item['correlation_id'],
                "data": dumps(item['data']),
                "updated": None,
                "status": item.get('status', 0),
                "rating": item.get('rating', 0),
                "meta": dumps(item['meta']),
                "customer": item['customer'],
                "parent": str(item.get('parent', '')),
                "company_id": item['company_id']
            })
            statement_insert_customer.bind_dict({
                "review_id": review_id,
                "correlation_id": item['correlation_id'],
                "customer": item['customer'],
                "company_id": item['company_id'],
                "parent": str(item.get('parent', '')),
                "is_moderated": False
            })
            batch.add_statement(statement_insert_review)
            batch.add_statement(statement_insert_customer)
        async with self.app.services.scylla.db as db:
            await db.session.execute_batch(batch)
        for review in list_to_moderate:
            await self.add_to_moderate_stream(*review)
        for review in list_to_cpannel:
            await self.app.services.producer_cpannel.notify(*review, "create")

    async def add_to_moderate_stream(self, review_id, company_id):
        await self.app.services.producer_moderator.send(namespace="review",
                                                        topic="moderating",
                                                        body={
                                                            "review_id": review_id,
                                                            "company_id": company_id,
                                                        })

    async def test(self, limit=1000):
        task_list = []
        for i in range(limit):
            task = self.create(**{"company_id": "apple",
                                  "customer": "macbook_user",
                                  "data": {"text": "12"},
                                  "meta": {"text": "12"},
                                  "rating": 3,
                                  "correlation_id": "macbook",
                                  "parent": ""
                                  })
            task_list.append(asyncio.create_task(
                asyncio.wait_for(task, timeout=10)))
        start_time = time.time()
        print(start_time)
        for i in task_list:
            await i
        stop_time = time.time()
        return stop_time - start_time

    async def export_by_dict(self, dict_data):
        list_correlation_id = list(dict_data['moderate'].keys())
        list_company_id = list(dict_data['no_moderate'].keys())
        list_review_id_m = []
        list_review_id_um = []

        for reviews_id in dict_data['moderate'].values():
            if isinstance(reviews_id, list):
                list_review_id_m.extend(reviews_id)
            if isinstance(reviews_id, uuid.UUID):
                list_review_id_m.append(reviews_id)
        for reviews_id in dict_data['no_moderate'].values():
            if isinstance(reviews_id, list):
                list_review_id_um.extend(reviews_id)
            if isinstance(reviews_id, uuid.UUID):
                list_review_id_um.append(reviews_id)
        response = []
        async with self.app.services.scylla.db as db:
            for list_review, list_p_key, statement in [(list_review_id_m, list_correlation_id,
                                                        self.prepared_statement["review_list_by_list_and_list"]),
                                                       (list_review_id_um, list_company_id,
                                                        self.prepared_statement["moderate_list_by_list_and_list"])]:
                if len(list_review) > 0:
                    select_review_list = statement.bind()
                    select_review_list.bind_list([list_p_key, list_review])
                    review_result = await db.session.execute(select_review_list)
                    for i in review_result:
                        review = i.as_dict()
                        response.append(return_review_type(review))
            return response


def unixtime_to_datetime(data: uuid) -> datetime.datetime:
    return datetime.datetime.fromtimestamp((data.time - 0x01b21dd213814000) * 100 / 1e9)


def result_to_review_list(result, data):
    if data is None:
        data = []
    for i in result:
        data.append(return_review_type(i.as_dict()))
    return data


def return_review_type(data: dict):
    data_r = dict()
    data_r["review_id"] = data["review_id"]
    data_r["correlation_id"] = data["correlation_id"]
    data_r["customer"] = data["customer"]
    data_r["rating"] = data["rating"]
    data_r["status"] = data["status"]
    data_r["company_id"] = data["company_id"]
    data_r["created"] = unixtime_to_datetime(data["review_id"])
    data_r["data"] = loads(data["data"])
    data_r["meta"] = loads(data["meta"])
    data_r["children"] = []
    if "company_id" in data:
        data_r["company_id"] = data["company_id"]
    if data["updated"] is not None:
        data_r["updated"] = unixtime_to_datetime(data["updated"])
    return data_r

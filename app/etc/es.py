import enum
from dataclasses import dataclass
from types import SimpleNamespace
from typing import TypedDict, Iterable, ClassVar, List, Union

import kaiju_tools.jsonschema as j
from kaiju_tools.serialization import Serializable
from kaiju_tools.exceptions import ValidationError

from app.etc.attributes import AttributePrototypes

__all__ = [
    'MAX_NUMBER_OF_SHARDS', 'MAX_RESULT_WINDOW_LIMIT', 'PRIMARY_KEY', 'BASE_MAPPING',
    'FIELD_SETTINGS', 'ANALYSIS', 'PROJ_VALUE_FIELD', 'ID_FIELD', 'LABEL_FIELD',
    'MIN_PREFIX_SIZE', 'MAX_PREFIX_SIZE', 'MIN_TEXT_QUERY_SIZE', 'QueryError',
    'ClusterSettings', 'Metadata', 'Mapping', 'Index', 'SearchRequest', 'BoostSettings'
]

# defaults

settings_schema = j.Object({
    'number_of_shards': j.Integer(minimum=1, default=1),
    'number_of_replicas': j.Integer(minimum=0, default=1),
    'refresh_interval': j.String(minLength=1, default='10s'),
    'max_result_window': j.Integer(minimum=1, default=25000),
    'max_inner_result_window': j.Integer(minimum=1, default=500),
    'max_regex_length': j.Integer(minimum=1, default=50),
    'mapping': j.Object({
        'total_fields.limit': j.Integer(minimum=1, default=5000),
        'nested_fields.limit': j.Integer(minimum=1, default=5000)
    }, default={'total_fields.limit': 5000, 'nested_fields.limit': 5000}),
})
_settings_schema = j.compile_schema(settings_schema)

MIN_PREFIX_SIZE = 1
MAX_PREFIX_SIZE = 6
MIN_TEXT_QUERY_SIZE = 4
MAX_NUMBER_OF_SHARDS = 128
MAX_RESULT_WINDOW_LIMIT = 100000
MAX_QUERY_STRING = 512
SORTING_FIELD = 'sort'
KEYWORD_SUBFIELD = 'kw'
FIRST_TOKEN_SUBFIELD = 'start'
ATTRIBUTES_FIELD = 'attributes'

TEXT_SORT_TOKEN = {
    'type': 'keyword',
    'normalizer': 'sorting'
}
TEXT_START_TOKEN = {
    'type': 'text',
    'analyzer': 'ngram',
    'index_options': 'docs',
    'norms': False
}
TEXT_KEYWORD_TOKEN = {
    'type': 'keyword',
    'normalizer': 'keyword'
}

FIELD_SETTINGS = {
    AttributePrototypes.numeric: {
        'type': 'scaled_float',
        'scaling_factor': 1000
    },
    AttributePrototypes.numeric_range: {
        'type': 'float_range'
    },
    AttributePrototypes.numeric_list: TEXT_KEYWORD_TOKEN,
    AttributePrototypes.string: TEXT_KEYWORD_TOKEN,
    AttributePrototypes.string_list: TEXT_KEYWORD_TOKEN,
    AttributePrototypes.text: {
        'type': 'text',
        # 'index_options': 'docs',
        'index_phrases': True,
        'analyzer': 'match',
        'fields': {
            SORTING_FIELD: TEXT_SORT_TOKEN,
            FIRST_TOKEN_SUBFIELD: TEXT_START_TOKEN
        }
    },
    '_aggregated_keyword': {
        'type': 'keyword',
        'normalizer': 'keyword',
        'eager_global_ordinals': True,
        'fields': {
            FIRST_TOKEN_SUBFIELD: TEXT_START_TOKEN
        }
    },
    '_text_with_filters': {
        'type': 'text',
        # 'index_options': 'docs',
        'index_phrases': True,
        'analyzer': 'match',
        'fields': {
            SORTING_FIELD: TEXT_SORT_TOKEN,
            FIRST_TOKEN_SUBFIELD: TEXT_START_TOKEN,
            KEYWORD_SUBFIELD: TEXT_KEYWORD_TOKEN
        }
    }
}

ANALYSIS = {
    'tokenizer': {
        'ngram': {
            'type': 'edge_ngram',
            'min_gram': MIN_PREFIX_SIZE,
            'max_gram': MAX_PREFIX_SIZE,
            'token_chars': [
                'letter',
                'digit',
                'custom'
            ],
            'custom_token_chars': '_'
        }
    },
    'char_filter': {
        'replace_punctuation': {
            'type': 'pattern_replace',
            'pattern': r'[\\|/+:*]',
            'replacement': '_'
        },
        'remove_special_chars': {
            'type': 'pattern_replace',
            'pattern': r'[^\p{L}\p{Nd}\s]',
            'replacement': ''
        },
        'select_prefix': {
          'type': 'pattern_replace',
          'pattern': f'^(.{{{MAX_PREFIX_SIZE}}})(.*)$',
          'replacement': '$1'
        }
    },
    'filter': {
        'ngram_token_filter': {
            'type': 'limit',
            'max_token_count': MAX_PREFIX_SIZE
        },
        'remove_short_words': {
            'type': 'length',
            'min': 3
        }
    },
    'normalizer': {
        'sorting':  {
            'type': 'custom',
            'char_filter': [
                'remove_special_chars',
                'select_prefix'
            ],
            'filter': [
                'trim',
                'lowercase',
                'asciifolding'
            ]
        },
        'keyword':  {
            'type': 'custom',
            'char_filter': [
                'replace_punctuation',
                'remove_special_chars'
            ],
            'filter': [
                'trim',
                'lowercase',
                'asciifolding'
            ]
        }
    },
    'analyzer': {
        'match': {
            'type': 'custom',
            'tokenizer': 'standard',
            'char_filter': [
                'replace_punctuation',
                'remove_special_chars'
            ],
            'filter': [
                'trim',
                'lowercase',
                'asciifolding',
                'remove_short_words'
            ]
        },
        'ngram': {
            'type': 'custom',
            'tokenizer': 'ngram',
            'char_filter': [
                'replace_punctuation',
                'remove_special_chars'
            ],
            'filter': [
                'trim',
                'lowercase',
                'asciifolding',
                'ngram_token_filter',
                'unique'
            ]
        }
    }
}

# base mapping

ID_FIELD = 'item'
PRIMARY_KEY = 'doc_id'
PROJ_VALUE_FIELD = 'value'
LABEL_FIELD = 'label'

BASE_MAPPING = {
    ID_FIELD: {
        'type': 'keyword',
        'index': False
    },
    'entity': {
        'type': 'keyword',
        'index': False
    },
    'attributes': {
        'type': 'keyword'
    }
}


class ClusterSettings(TypedDict):
    """ES settings for an entire cluster.

    See `https://www.elastic.co/guide/en/elasticsearch/reference/current/modules.html`_
    """

    persistent: dict
    transient: dict


@dataclass
class Metadata:
    """Index settings in this application.

    >>> meta = Metadata(max_number_of_shards=1, max_result_window_limit=1000, indices_prefix='entity')
    """

    cluster_settings: ClusterSettings = None
    max_number_of_shards: int = MAX_NUMBER_OF_SHARDS
    max_result_window_limit: int = MAX_RESULT_WINDOW_LIMIT
    indices_prefix: str = None


@dataclass
class Mapping(Serializable):
    """Index fields mapping."""

    class DynamicMode(enum.Enum):
        """Index fields mode.

        See `https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic.html`_
        """

        enabled = True
        disabled = False
        strict = 'strict'

    term_types: ClassVar = {
        'keyword', 'boolean'
    }  #: permitted for term filters and aggregation and sorting

    wildcard_types: ClassVar = {
        'keyword'
    }  #: permitted for wildcard filters (*)

    text_search_types: ClassVar = {
        'text'
    }  #: permitted for text search and analysis

    number_types: ClassVar = {
        'long', 'integer', 'short', 'byte', 'double', 'float', 'half_float',
        'scaled_float'
    }  #: permitted for comparison queries and sorting

    ranged_types: ClassVar = {
        'integer_range', 'float_range', 'long_range', 'double_range',
        'date_range', 'ip_range', 'date'
    }  #: permitted for range comparison queries

    ranged_and_number_types: ClassVar = number_types ^ ranged_types
    term_ranged_and_number_types: ClassVar = term_types ^ ranged_and_number_types

    range_operators: ClassVar = {
        'gt', 'lt', 'gte', 'lte', 'relation'
    }  #: permitted set of range operators

    range_operator_relations: ClassVar = {
        'INTERSECTS', 'CONTAINS', 'WITHIN'
    }  #: permitted relation query values for ranged types

    wildcard_operator = 'wildcard'
    not_operator = 'not'
    exists_operator = 'exists'

    metadata: Metadata
    index: 'Index'
    properties: dict
    dynamic: DynamicMode = DynamicMode.strict.value
    _meta: Union[dict, SimpleNamespace] = None

    def __post_init__(self):
        self._meta = self._filter_meta_field(self._meta)

    @staticmethod
    def _filter_meta_field(_meta: dict) -> SimpleNamespace:
        """Convert meta dictionary into a more convenient namespace object."""

        def _init_value(v):
            if isinstance(v, (tuple, list)):
                v = frozenset(str(key) for key in v)
            elif isinstance(v, dict):
                for _k, _v in v.items():
                    _k = str(_k)
                    if not _k.startswith('_'):
                        v[_k] = _init_value(_v)
                v = SimpleNamespace(**v)
            return v

        if _meta is None:
            _meta = {}
        elif isinstance(_meta, SimpleNamespace):
            return _meta

        return _init_value(_meta)

    @property
    def meta(self) -> SimpleNamespace:
        return self._meta

    def repr(self) -> dict:
        return {
            'dynamic': self.dynamic,
            '_meta': dict(self._meta.__dict__),
            'properties': self.properties
        }


@dataclass
class Index(Serializable):
    """ES index object representation."""

    name: str
    metadata: Metadata
    mappings: dict
    aliases: Iterable[str] = None
    settings: dict = None

    def __post_init__(self):
        self.aliases = self._init_aliases(self.aliases)
        self.mapping = Mapping(metadata=self.metadata, index=self, **self.mappings)
        index_settings = self.settings.get('index')
        if not index_settings:
            index_settings = {}
        self.index_settings = _settings_schema(index_settings)
        analysis = self.settings.get('analysis')
        self.analysis = analysis

    @property
    def alias(self) -> str:
        """Returns a first available index alias."""
        return next(iter(self.aliases)) if self.aliases else self.name

    @property
    def primary_key(self) -> str:
        return PRIMARY_KEY

    def repr(self):
        return {
            'aliases': self.aliases,
            'mappings': self.mapping.repr(),
            'settings': {
                'index': self.index_settings,
                'analysis': self.analysis
            }
        }

    @staticmethod
    def _init_aliases(aliases):
        if aliases is None:
            return
        elif isinstance(aliases, dict):
            return aliases
        elif isinstance(aliases, Iterable):
            return {alias: {} for alias in aliases}
        else:
            raise ValueError('Invalid alias: %s.' % aliases)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __truediv__(self, other):
        return f'{self}/{other}'


class QueryError(ValidationError):
    """Error while constructing a search query."""


@dataclass
class BoostSettings:
    strict: float = 32.0
    prefix_strict: float = 16.0
    prefix_phrase: float = 8.0
    match: float = 2.0
    match_fuzzy: float = 1.0


@dataclass
class SearchRequest(Serializable):
    """ES query object."""

    idx: Index
    conditions: dict = None
    projections: dict = None
    collapse_field: str = None
    sort: list = None
    offset: int = 0
    limit: int = 0
    query_string: str = None
    text_search_fields: List[str] = None
    min_score: float = 0.
    boost_settings: BoostSettings = None

    _count_agg: ClassVar = '_count'

    class _Query(TypedDict):
        nested: bool
        proj: list
        filter: list
        must: list
        must_not: list

    def __post_init__(self):
        if self.projections is None:
            self.projections = {}
        if type(self.sort) not in {list, tuple, type(None)}:
            self.sort = [self.sort]
        if self.query_string:
            self.query_string = self.query_string.strip().lower()[:MAX_QUERY_STRING]
        self._text_search_enabled = self.text_search_fields and self.query_string
        if self._text_search_enabled and self.boost_settings is None:
            self.boost_settings = BoostSettings()
        self._query_enabled = self._text_search_enabled or self.conditions
        self._properties = self.idx.mapping.properties
        self._entity = self.idx.mapping.meta.entity
        if self.collapse_field:
            self._collapse_attr = self.get_attr(self.collapse_field)
            self._collapse_field = self._get_collapse_field_name(self.collapse_field, self._collapse_attr)
        else:
            self._collapse_attr = None
            self._collapse_field = None
        self._request = self._create_request()

    def _create_request(self) -> dict:
        """Create search request body."""
        aggs = {}
        request = {
            'from': self.offset,
            'size': self.limit,
            '_source': False,
            'min_score': self.min_score
        }
        query = self._Query(
            nested=False,
            must=[],
            must_not=[],
            filter=[],
            proj=[]
        )
        nested_queries = {}

        self._init_q_collapse(request, aggs)
        self._init_q_condition(query, nested_queries)
        self._init_q_text_search(query, nested_queries)
        self._init_q_sort(request, nested_queries)

        if aggs:
            request['aggs'] = aggs

        # collecting nested filters into a single query

        for key, nested in nested_queries.items():
            proj = nested['proj']
            must = nested['must']
            must_not = nested['must_not']
            filters = nested['filter']
            filters.extend(proj)

            if not any((must, must_not, filters)):
                continue

            if must:
                query['must'].append({
                    'nested': {
                        'path': key,
                        'query': {
                            'dis_max': {
                                'queries': must,
                                'tie_breaker': 0
                            }
                        }
                    }
                })

            nested_query = {}

            if filters:
                nested_query['filter'] = filters

            if must_not:
                nested_query['must_not'] = must_not

            nested_query = {
                'nested': {
                    'path': key,
                    'query': {
                        'bool': nested_query
                    }
                }
            }

            query['filter'].append(nested_query)

        # query postprocessing and optimization

        if query['must']:
            query['must'] = {
                'dis_max': {
                    'queries': query['must'],
                    'tie_breaker': 0
                }
            }

        query = {k: v for k, v in query.items() if v}

        if query:
            request['query'] = {
                'bool': query
            }
        else:
            request['query'] = {
                'match_all': {}
            }

        return request

    def _init_q_collapse(self, request: dict, aggs: dict) -> None:
        """Init collapse query."""
        if not self.collapse_field:
            return

        request['collapse'] = {
            'field': self._collapse_field
        }
        aggs[self._count_agg] = {
            'cardinality': {
                'field': self._collapse_field
            }
        }

    def _init_q_condition(self, query: dict, nested_queries: dict) -> None:
        """Init filters and other conditions."""
        if not self._query_enabled:
            return

        if not self.conditions:
            return

        for key, condition in self.conditions.items():
            attr = self.get_attr(key)

            query['filter'].append({
                'term': {
                    ATTRIBUTES_FIELD: key
                }
            })

            if self.is_nested(attr):
                nested_query = self._init_nested_query(key, attr, nested_queries)
                key = self.get_value_field_name(key, attr)
                attr = attr['properties'][PROJ_VALUE_FIELD]
                self._init_condition_filter(nested_query, key, attr, condition)
                continue

            self._init_condition_filter(query, key, attr, condition)

    def _init_condition_filter(self, query: dict, key: str, attr: dict, condition) -> None:
        attr_type = attr['type']

        if type(condition) is dict and Mapping.not_operator in condition:
            condition = condition[Mapping.not_operator]
            not_ = True
        else:
            not_ = False

        q = None

        if type(condition) is dict:
            if Mapping.wildcard_operator in condition:
                wildcard = condition[Mapping.wildcard_operator]
                q = self._create_wildcard_filter(key, wildcard, attr_type)
            elif Mapping.exists_operator in condition:
                # it's a hack since nested fields should act differently for exist queries
                if query.get('nested'):
                    if not_:
                        q = {
                            'bool': {
                                'filter': query['proj']
                            }
                        }
                        query['proj'] = []
                else:
                    q = self._create_exists_filter(key)
            else:
                q = self._create_range_filter(key, condition, attr_type)
        else:
            q = self._create_term_filter(key, condition, attr_type, attr)

        if q:
            if not_:
                query['must_not'].append(q)
            else:
                query['filter'].append(q)

    def _init_q_text_search(self, query: dict, nested_queries: dict) -> None:
        """Init text query."""
        if not self._text_search_enabled:
            return

        for key in self.text_search_fields:
            attr = self.get_attr(key)
            if self.is_nested(attr):
                nested_query = self._init_nested_query(key, attr, nested_queries)
                key = self.get_value_field_name(key, attr)
                attr = attr['properties'][PROJ_VALUE_FIELD]
                self._init_text_search_filter(nested_query, key, attr['type'], attr)
                continue

            self._init_text_search_filter(query, key, attr['type'], attr)

    def _init_text_search_filter(self, query: dict, key: str, attr_type: str, attr: dict) -> None:
        """Create text search query."""
        _query = query['must']

        if attr_type in Mapping.text_search_types:
            if len(self.query_string) <= MAX_PREFIX_SIZE:
                _query.append({
                    'constant_score': {
                        'filter': {
                            'match': {
                                f'{key}.{FIRST_TOKEN_SUBFIELD}': {
                                    'query': self.query_string,
                                    'operator': 'and'
                                }
                            }
                        },
                        'boost': self.boost_settings.prefix_strict
                    }
                })
            if len(self.query_string) >= MIN_TEXT_QUERY_SIZE:
                _query.extend([
                    {
                        'constant_score': {
                            'filter': {
                                'match_phrase': {
                                    key: {
                                        'query': self.query_string
                                    }
                                }
                            },
                            'boost': self.boost_settings.prefix_phrase,
                        }
                    },
                    {
                        'constant_score': {
                            'filter': {
                                'match': {
                                    key: {
                                        'query': self.query_string,
                                        'operator': 'and',
                                        'boost': self.boost_settings.match
                                    }
                                }
                            },
                            'boost': self.boost_settings.match,
                        }
                    },
                    {
                        'match': {
                            key: {
                                'query': self.query_string,
                                'operator': 'and',
                                'max_expansions': 20,
                                'fuzziness': 'AUTO',
                                'prefix_length': 3,
                                'boost': self.boost_settings.match_fuzzy
                            }
                        }
                    }
                ])

        elif attr_type in Mapping.wildcard_types:
            if FIRST_TOKEN_SUBFIELD in attr.get('fields', {}):
                _query.extend([
                    {
                        'constant_score': {
                            'filter': {
                                'match': {
                                    f'{key}.{FIRST_TOKEN_SUBFIELD}': {
                                        'query': self.query_string,
                                        'operator': 'and'
                                    }
                                }
                            },
                            'boost': self.boost_settings.prefix_strict
                        }
                    },
                    {
                        'constant_score': {
                            'filter': {
                                'term': {
                                    key: self.query_string
                                }
                            },
                            'boost': self.boost_settings.strict
                        }
                    }
                ])
            else:
                _query.extend([
                    {
                        'constant_score': {
                            'filter': {
                                'prefix': {
                                    key: {
                                        'value': self.query_string
                                    }
                                }
                            },
                            'boost': self.boost_settings.prefix_strict
                        }
                    },
                    {
                        'constant_score': {
                            'filter': {
                                'term': {
                                    key: self.query_string
                                }
                            },
                            'boost': self.boost_settings.strict
                        }
                    }
                ])
        else:
            raise QueryError('Attribute is not permitted for text search: "%s" of type "%s".' % (key, attr_type))

    def _init_q_sort(self, request: dict, nested_queries: dict) -> None:
        """Init sorting."""
        if not self.sort:
            return

        request['sort'] = _sort = []

        for row in self.sort:
            if type(row) is str:
                key, order = row, 'asc'
            else:
                key, order = row['key'], row.get('order', 'asc')

            attr = self.get_attr(key)

            if self.is_nested(attr):
                q = {
                    'path': key
                }
                nested_query = self._init_nested_query(key, attr, nested_queries)
                if nested_query['filter'] or nested_query['proj']:
                    q['filter'] = {
                        'bool': {
                            'filter': nested_query['filter'] + nested_query['proj']
                        }
                    }
                q = {
                    self._get_sorting_field_name(key, attr): {
                        'order': order,
                        'nested': q
                    }
                }
            else:
                q = {
                    self._get_sorting_field_name(key, attr): order
                }

            _sort.append(q)

    def _init_nested_query(self, key: str, attr: dict, nested_queries: dict) -> _Query:
        if key in nested_queries:
            q = nested_queries[key]
        else:
            proj = []
            for sub_key, attr in attr['properties'].items():
                if sub_key in self.projections:
                    q = self._create_term_filter(f'{key}.{sub_key}', self.projections[sub_key], attr['type'], attr)
                    proj.append(q)
            nested_queries[key] = q = self._Query(
                nested=True,
                must=[],
                must_not=[],
                filter=[],
                proj=proj
            )
        return q

    def get_result(self, response: dict) -> (int, list):
        """Parse ES response and return expected results."""
        data = response['hits']['hits']
        if self.collapse_field:
            count = response['aggregations'][self._count_agg]['value']
            data = [
                row['fields'][self._collapse_field][0]
                for row in data
                if row.get('fields')
            ]
        else:
            count = response['hits']['total']['value']
            data = [row['_id'] for row in data]
        return count, data

    def repr(self) -> dict:
        return self._request

    def get_attr(self, key: str) -> dict:
        """Get attribute properties by key name."""
        attr = self._properties.get(key)
        if not attr:
            raise QueryError('No such field "%s" in index "%s".' % (key, self._entity))
        return attr

    @classmethod
    def get_value_field_name(cls, key: str, attr: dict) -> str:
        if cls.is_nested(attr):
            key = f'{key}.{PROJ_VALUE_FIELD}'
        return key

    @classmethod
    def _get_collapse_field_name(cls, key: str, attr: dict) -> str:
        key = cls.get_value_field_name(key, attr)
        key = cls._get_term_field_name(key, attr)
        return key

    @classmethod
    def _get_term_field_name(cls, key: str, attr: dict) -> str:
        fields = attr.get('fields')
        if fields and KEYWORD_SUBFIELD in fields:
            key = f'{key}.{KEYWORD_SUBFIELD}'
        return key

    @classmethod
    def _get_sorting_field_name(cls, key: str, attr: dict) -> str:
        key = cls.get_value_field_name(key, attr)
        fields = attr.get('fields')
        if fields and SORTING_FIELD in fields:
            key = f'{key}.{SORTING_FIELD}'
        return key

    @staticmethod
    def is_nested(attr: dict) -> bool:
        return attr['type'] == 'nested'

    @staticmethod
    def _create_term_filter(key: str, value: Union[str, List[str]], attr_type: str, attr: dict) -> dict:
        if attr_type in Mapping.text_search_types:
            if 'fields' in attr and KEYWORD_SUBFIELD in attr['fields']:
                key = f'{key}.{KEYWORD_SUBFIELD}'
            else:
                raise QueryError('Attribute does not support term filter: "%s".' % key)
        elif attr_type not in Mapping.term_ranged_and_number_types:
            raise QueryError('Attribute does not support term filter: "%s".' % key)
        if type(value) in {list, tuple, set, frozenset}:
            return {
                'terms': {
                    key: value
                }
            }
        else:
            return {
                'term': {
                    key: value
                }
            }

    @staticmethod
    def _create_exists_filter(key: str) -> dict:
        return {
            'exists': {
                'field': key
            }
        }

    @staticmethod
    def _create_wildcard_filter(key: str, value: str, attr_type: str) -> dict:
        if attr_type not in Mapping.wildcard_types:
            raise QueryError('Attribute does not support wildcard filter: "%s".' % key)
        # trying to optimize whenever possible
        if value.endswith('*') and '*' not in value.rstrip('*'):
            return {
                'prefix': {
                    key: {
                        'value': value.rstrip('*').lower()
                    }
                }
            }
        else:
            return {
                'wildcard': {
                    key: {
                        'value': value.lower()
                    }
                }
            }

    @staticmethod
    def _create_range_filter(key: str, value: dict, attr_type: str) -> dict:
        if attr_type not in Mapping.ranged_and_number_types:
            raise QueryError('Attribute does not support range filter: "%s".' % key)
        relation = value.get('relation')
        if relation:
            if attr_type not in Mapping.ranged_types:
                raise QueryError('Attribute does not support range filter with relation: "%s".' % key)
            if relation not in Mapping.range_operator_relations:
                raise QueryError('Invalid relation condition for a range filter: "%s".' % key)
        return {
            'range': {
                key: value
            }
        }

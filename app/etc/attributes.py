
__all__ = ['AttributePrototypes']


class AttributePrototypes:
    numeric = 'numeric'
    numeric_range = 'numeric_range'
    string = 'string'
    numeric_list = 'numeric_list'
    text = 'text'
    string_list = 'string_list'
    json = 'json'

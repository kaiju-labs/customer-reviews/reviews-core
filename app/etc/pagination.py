import abc
from functools import wraps
from typing import Any, Callable, Coroutine, TypedDict

__all__ = [
    'PaginatedResult', 'paginated', 'PER_PAGE', 'MAX_PAGE_SIZE'
]

PER_PAGE = 24
MAX_PAGE_SIZE = 5000


class _Pagination(TypedDict):
    page: int  # current page
    pages: int  # total number of pages
    per_page: int  # max doc count on a single page
    count: int  # total doc count


class PaginatedResult(TypedDict):
    """Result dict returned by a pagination decorator (type hint)."""
    pagination: _Pagination
    data: Any


class PaginatedFunction(Callable, abc.ABC):
    """A paginated async function (type hint interface, not for use)."""

    def __call__(self, *args, offset: int = None, limit: int = None, **kws) -> Coroutine[(int, Any, Any)]:
        ...


class PaginatedWrapper(Callable, abc.ABC):
    """Wrapper interface returned by `paginated` decorator (type hint interface, not for use)."""

    def __call__(self, *args, page: int = 1, per_page: int = PER_PAGE, **kws) -> Coroutine[PaginatedResult, Any, Any]:
        ...


def paginated(f: PaginatedFunction) -> PaginatedWrapper:
    """Convert pages to limit/offset (use it as a decorator).

    :param f: async function, must return a tuple of (total objects count, function result)

    Example:

    .. code-block:: python

        @paginated
        async def my_page_function(*args, limit: int, offset: int, **kws):
            ...

    becomes wrapped and equivalent to:

    .. code-block:: python

        async def my_page_function(*args, page: int, per_page: int, **kws):
            ...

    The function result is also wrapped and returned in the 'data' field:

    .. code-block:: python

        {
            'pagination': {
                'page': page,
                'pages': pages,
                'per_page': per_page,
                'count': count
            },
            'data': result
        }
    """

    @wraps(f)
    async def _wrapper(*args, page: int = 1, per_page: int = PER_PAGE, **kws) -> PaginatedResult:
        page = max(1, page)
        per_page = min(max(1, per_page), MAX_PAGE_SIZE)
        limit = per_page
        offset = (page - 1) * per_page
        count, result = await f(*args, limit=limit, offset=offset, **kws)
        pages = max(1, count // per_page + bool(count % per_page))
        return {
            'pagination': {
                'page': page,
                'pages': pages,
                'per_page': per_page,
                'count': count
            },
            'data': result
        }

    return _wrapper

from app.services.cassandra.CORE import *
from app.services.cassandra.CORE_type import *

reviews = Table("reviews",
                Columns("parent", TEXT(), clustering_key=True, order="DESC"),
                Columns("review_id", TIMEUUID(),
                        clustering_key=True, order="DESC"),
                Columns("correlation_id", TEXT(), partition_key=True),
                Columns("company_id", TEXT()),
                Columns("data", TEXT()),
                Columns("rating", INT()),
                Columns("updated", TIMEUUID()),
                Columns("status", INT()),
                Columns("meta", TEXT()),
                Columns("customer", TEXT()),
                Columns("children", LIST(TEXT()))
                )

moderating = Table("moderating_reviews",
                   Columns("company_id", TEXT(), partition_key=True),
                   Columns("review_id", TIMEUUID(),
                           clustering_key=True, order="DESC"),
                   Columns("correlation_id", TEXT()),
                   Columns("parent", TEXT()),
                   Columns("children", LIST(TEXT())),
                   Columns("data", TEXT()),
                   Columns("rating", INT()),
                   Columns("updated", TIMEUUID()),
                   Columns("status", INT()),
                   Columns("meta", TEXT()),
                   Columns("customer", TEXT()),
                   Columns("moderator_status", TEXT())
                   )

customer_reviews = Table("customer_reviews",
                         Columns("customer", TEXT(), partition_key=True),
                         Columns("review_id", TIMEUUID(),
                                 clustering_key=True, order="DESC"),
                         Columns("company_id", TEXT()),
                         Columns("correlation_id", TEXT()),
                         Columns("parent", TEXT()),
                         Columns("is_moderated", BOOLEAN())
                         )

ratings = Table("ratings",
                Columns("correlation_id", TEXT(), partition_key=True),
                Columns("rating", INT(), clustering_key=True, order="DESC"),
                Columns("value", COUNTER()),
                )

settings_auto = Table("settings_moderator",
                      Columns("name_moderator", TEXT(), partition_key=True),
                      Columns("setting_name", TEXT(),
                              clustering_key=True, order="DESC"),
                      Columns("value", TEXT()),
                      )


indices = Table('indices',
                Columns('id', TEXT(), partition_key=True),
                Columns('name', TEXT(), clustering_key=True, order="DESC"),
                Columns('settings', TEXT())
                )

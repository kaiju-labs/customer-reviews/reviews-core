"""
Write your routes here.

`urls` - url list which will be registered in the app's router
"""

from kaiju_tools.http.views import JSONRPCView


urls = [
    ('*', '/public/rpc', JSONRPCView, 'json_rpc_view')
]

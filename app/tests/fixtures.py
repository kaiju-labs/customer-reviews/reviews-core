"""
Place your pytest fixtures here.
"""
import pytest

from aiohttp import web
from pytest_aiohttp.plugin import aiohttp_server

import types
from types import SimpleNamespace
from typing import *

from kaiju_tools.init_app import init_app, ConfigSettings, init_config
from kaiju_tools.docker import DockerStack
from app.application import init_app
from kaiju_tools.tests.fixtures import *
# from kaiju_db.tests.fixtures import *


@pytest.fixture
def app_stack(application, logger):
    """All services including the app itself."""
    with DockerStack(
        name='pytest-stack',
        compose_files=['./docker-compose.yaml', './docker-compose-pytest.yaml'],
        profiles=['full'],
        build=True,
        app=application(),
        logger=logger
    ) as stack:
        yield stack


@pytest.fixture
async def app_start(aiohttp_server):
    command, config = init_config()
    settings: ConfigSettings = config.settings
    app = init_app(settings)

    server = await aiohttp_server(app)
    yield app
    await server.close()



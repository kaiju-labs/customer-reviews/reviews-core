import aiohttp

import asyncio
from kaiju_tools.encoding.json import dumps
import time

# for i in range(100):


async def make_create(session):
    result = await session.request("POST", "http://0.0.0.0:8791/public/rpc", data=dumps({
        "method": "review.moderate_list",
        "params": {
            "company_id": "apple",
        }
    }))
    return await result.text()


async def main():
    async with aiohttp.ClientSession() as session:
        task_list = []
        result_list = []
        start_time = time.monotonic()
        number_requests = 100000
        for i in range(number_requests):
            task = make_create(session)
            task_list.append(asyncio.create_task(asyncio.wait_for(task, timeout=100)))
        count = 0
        try:
            for i in task_list:
                result_list.append(await i)
            stop_time = time.monotonic()
            print((stop_time-start_time)/number_requests)
            print(len(result_list))
        except:
            print(len(result_list))

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

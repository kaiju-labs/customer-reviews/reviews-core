"""
Tests for abstract and base classes and interfaces.
"""

from .fixtures import *
import uuid
import time


async def test_nothing(app_start):
    """Just a stub."""
    self = app_start.services.Messenger
    user_id = "7701a820-a47c-11ec-8049-09c1c7645844"
    user_id1 = "7702a820-a47c-11ec-8049-09c1c7645844"
    await self.create_chat(user_id, "TEST3")
    chat_id = await self.chat.list(user_id)
    chat_id = chat_id['data'][0]['chat_id']
    start_time = time.perf_counter()
    temp_uuid_2 = uuid.uuid4()
    for i in range(0, 120):
        await self.send_message(user_id, chat_id, str(i), [{"attachment": temp_uuid_2, "tags": ['a', 'b']}])
    time_sending_120_message = (time.perf_counter() - start_time) * 1000
    time_sending_average = time_sending_120_message / 120
    start_time = time.perf_counter()
    for i in range(120, 1040):
        await self.send_message(user_id1, chat_id, str(i))
    time_sending_120_message_without = (time.perf_counter() - start_time) * 1000
    time_sending_without_average = time_sending_120_message_without / 920
    message_list0 = await self.message.list(user_id, chat_id, paging=False)
    message_list1 = await self.message.list(user_id, chat_id, paging=True)
    start_time = time.perf_counter()
    count = 1
    temp_message_list = await self.message.list(user_id, chat_id, paging=False)
    dict_data_testing_time = [temp_message_list["data"][0]["data"]]
    count += len(temp_message_list["data"])
    last_message_info = 0
    while temp_message_list["status"] != "FINISH":
        count += 1
        temp_message_list = await self.message.list(user_id, chat_id, paging=True)
        if temp_message_list["data"]:
            last_message_info = temp_message_list["data"][0]["message_info"]
            dict_data_testing_time.append(temp_message_list["data"][0]["data"])
    time_receiving_average = (time.perf_counter() - start_time) * 1000 / count
    start_time = time.perf_counter()
    dict_data_testing_time_1 = []
    last_message_info_local = message_list0["data"][0]["message_info"]
    for i in range(800):
        temp_message_list = await self.message.list_by_message_id(chat_id, last_message_info_local, direction=True)
        if temp_message_list["data"]:
            last_message_info_local = temp_message_list["data"][0]["message_info"]
            dict_data_testing_time_1.append(temp_message_list["data"][0]["data"])
    time_receiving_id_average_1 = (time.perf_counter() - start_time) * 1000 / 800
    last_message_info_local = last_message_info
    dict_data_testing_time_2 = []
    list_of_message_info = []
    start_time = time.perf_counter()
    for i in range(800):
        temp_message_list = await self.message.list_by_message_id(chat_id, last_message_info_local, direction=False)
        if temp_message_list["data"]:
            last_message_info_local = temp_message_list["data"][0]["message_info"]
            list_of_message_info.append(last_message_info_local)
            dict_data_testing_time_2.append(temp_message_list["data"][0]["data"])
    time_receiving_id_average_2 = (time.perf_counter() - start_time) * 1000 / 800
    message_list2 = await self.message.list_by_message_id(chat_id, message_list1["data"][0]["message_info"],
                                                          direction=True)
    message_list3 = await self.message.list_by_message_id(chat_id, message_list1["data"][0]["message_info"],
                                                          direction=False)
    chat_list = await self.chat.list(user_id)
    attachment_list = await self.attachment.list(user_id, chat_id, paging=False)
    attachment_list1 = await self.attachment.list(user_id, chat_id, paging=True)
    invite_list = None
    chat_list1 = None
    chat_list3 = None
    if user_id1 is not None:
        await self.chat.invite(chat_id, user_id1, user_id)
        invite_list = await self.chat.invite_list(user_id1)
        await self.chat.enter(user_id1, chat_id)
        chat_list1 = await self.chat.list(user_id1)
        await self.chat.update_name(chat_id, "test chat name")
        chat_list3 = await self.chat.list(user_id1)

    start_time = time.perf_counter()
    for i in list_of_message_info:
        await self.message.mark_as_read(chat_id, user_id, i)
    (time.perf_counter() - start_time) * 1000 / len(list_of_message_info)
    await self.message.mark_as_arrived(chat_id, message_list1["data"][0]["message_info"])
    await self.message.mark_as_read(chat_id, user_id, message_list1["data"][0]["message_info"])
    await self.message.mark_as_read(chat_id, user_id, message_list1["data"][0]["message_info"])
    await self.message.update(chat_id, message_list1["data"][0]["message_info"], "test")
    await self.message.list(user_id, chat_id, paging=False)
    message_list4 = await self.message.list(user_id, chat_id, paging=True)
    updated_message = message_list4["data"][0]
    chat_list2 = await self.chat.list(user_id)
    await self.chat.leave(chat_id, user_id1)
    chat_list4 = await self.chat.list(user_id1)
    await self.delete_chat(chat_id)
    chat_list5 = await self.chat.list(user_id)
    message_list6 = await self.message.list(user_id, chat_id, paging=True)
    attachment_list2 = await self.attachment.list(user_id, chat_id, paging=True)
    assert len(message_list0['data']) != 0
    self.logger.info(f"message_without_pagination OK {len(message_list0['data'])}")
    assert len(message_list1["data"]) != 0
    self.logger.info(f"message_with_pagination OK {len(message_list1['data'])}")
    assert len(message_list2["data"]) != 0
    self.logger.info(f"message_by_id_direction_1 OK {len(message_list2['data'])}")
    assert len(message_list3["data"]) != 0
    self.logger.info(f"message_by_id_direction_0 OK {len(message_list3['data'])}")
    assert len(chat_list['data']) != 0
    self.chat.logger.info(f"chat_list_user_id OK {chat_list['data'][0]['name'] }")
    assert len(attachment_list["data"]) != 0
    self.attachment.logger.info(f"attachments_without_pagination OK {len(attachment_list['data'])}")
    assert len(attachment_list1["data"]) != 0
    self.attachment.logger.info(f"return_attachments_with_pagination OK {len(attachment_list1['data'])}")
    assert len(invite_list['data']) != 0
    self.chat.logger.info(f"invite_list OK")
    assert len(chat_list1['data']) != 0
    self.chat.logger.info(f"chat_list_after_chat_name_user1 OK")
    assert len(chat_list2['data']) != 0
    self.chat.logger.info(f"chat_list_after_leave_chat_user1 OK")
    assert len(message_list4["data"]) != 0
    self.logger.info(f"message_list_after_updated_message OK")
    assert len(updated_message["data"]) != 0
    self.logger.info(f"updated_message OK")
    assert chat_list3['data'][0]['name'] != 'TEST3'
    self.chat.logger.info(f"chat_list_after_chat_name_user1 OK")
    # assert len(chat_list4['data']) == 0
    # self.chat.logger.info(f"chat_list_after_leave_chat_user1 OK")
    # assert len(chat_list5['data']) == 0
    # self.chat.logger.info(f"chat_list_after_delete_chat_user OK")
    # assert len(message_list6["data"]) == 0
    # self.logger.info(f"message_list_after_delete_chat OK {message_list6['data']}")
    # assert len(attachment_list2["data"]) == 0
    # self.logger.info(f"attachment_list_after_delete_chat OK")


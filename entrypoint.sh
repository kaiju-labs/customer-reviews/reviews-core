#!/usr/bin/env sh

set -e


wait_until_db() {
    until curl  -Xs "http://${SCYLLA_HOST}:9042" 2>&1 | grep -q allowed
    do
        sleep 5
        echo "Waiting for SCYLLA-DB at ${SCYLLA_HOST}"
    done
}
wait_until_db
if [ -n "${DEBUG}" ];
then
  echo '--- CONTAINER DEBUG INFORMATION ---'
  python3 --version
  echo '--- packages:'
  pip freeze
  echo '--- files:'
  ls .
  echo '-----------------------------------'
fi

python3 -OOm app "$@" --debug

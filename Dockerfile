FROM --platform=linux/amd64 ubuntu:20.04 as python-base


ENV PORT 8700
ENV PYTHONUNBUFFERED 1
ENV PYTHONOPTIMIZE 2
WORKDIR /app
ENTRYPOINT ["./entrypoint.sh"]
EXPOSE ${PORT}
STOPSIGNAL SIGINT

RUN apt-get update && \
    apt-get install -y python3 && \
    apt-get install -y python3-pip  && \
    rm -r /usr/lib/python*/lib2to3 && \
    rm -rf /var/lib/apt/lists/*


FROM python-base AS dependencies

COPY requirements.ubuntu requirements.ubuntu.build ./
RUN apt-get update && \
    apt-get install -y $(cat requirements.ubuntu | grep -v '#' | xargs) && \
    apt-get install -y $(cat requirements.ubuntu.build | grep -v '#' | xargs) && \
    rm -rf /var/lib/apt/lists/*

FROM dependencies as requirements

COPY requirements.txt ./
RUN pip3 install --no-cache --no-cache-dir -r requirements.txt

FROM requirements as code

COPY entrypoint.sh ./
COPY fixtures ./fixtures
COPY app/ ./app
COPY settings/ ./settings
COPY docs/ ./docs
RUN find / -type d -name __pycache__ -exec rm -r {} +
